Json Generator (Hlidac Shopu)
=============

Ze vstupni tabulky / tabulek vygeneruje json soubory `$shop.tld$/$slug$/price-history.json`
nebo `$shop.tld$/$slug$/metadata.json`

Pouziti s S3 extraktorem jako jediny `before` processor. Example:

```json
{
  "parameters": {
    "prefix": "items/"
  },
  "storage": {
    "input": {
      "tables": [
        {
          "source": "in.c-test.metadata",
          "destination": "shop_metadata.csv"
        }
      ]
    }
  },
  "processors": {
    "before": [
      {
        "definition": {
          "component": "kds-team.processor-json-generator-hlidac-shopu"
        },
        "parameters": {
          "format": "metadata"
        }
      }
    ]
  }
}
```

Vysledkem prikladu jsou soubory na ceste `items/$shop.tld$/$slug$/metadata.json`

[TOC]


Configuration
=============

Prijima jeden parametr `format` (hodnoty `metadata` nebo `pricehistory`)

Kazda vsuptni tabulka musi obsahovat sloupce `shop_id` a `slug`.

### Price History

**Konfigurace**

```json
 {
  "definition": {
    "component": "kds-team.processor-json-generator-hlidac-shopu"
  },
  "parameters": {
    "format": "pricehistory"
  }
}
```

Ocekava tabulku se sloupci `shop_id`, `slug` a `json`

JSON string ve sloupci `json` se ulozi podle nasledujici masky:
`$shop.tld$/$slug$/price-history.json`

### Metadata

**Konfigurace**

```json
 {
  "definition": {
    "component": "kds-team.processor-json-generator-hlidac-shopu"
  },
  "parameters": {
    "format": "metadata"
  }
}
```

Ocekava tabulku minimanlne se sloupci `shop_id`, `slug`.

Ostatni sloupce se pouziji jako JSON atributy. Vysledek se ulozi podle nasledujici masky:
`$shop.tld$/$slug$/metadata.json`

# Development

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path in
the `docker-compose.yml` file:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Clone this repository, init the workspace and run the component with following command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git clone git@bitbucket.org:kds_consulting_team/kds-team.processor-json-generator-hlidac-shopu.git kds-team.processor-json-generator-hlidac-shopuk
cd kds-team.processor-json-generator-hlidac-shopuk
docker-compose build
docker-compose run --rm dev
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Run the test suite and lint check using this command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose run --rm test
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Integration
===========

For information about deployment and integration with KBC, please refer to the
[deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/)
